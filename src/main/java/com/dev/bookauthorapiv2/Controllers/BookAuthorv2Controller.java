package com.dev.bookauthorapiv2.Controllers;
import com.dev.bookauthorapiv2.Book;
import com.dev.bookauthorapiv2.Author;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class BookAuthorv2Controller {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getBooks() {
        // tạo 6 tác giả đầy đủ thông tin
        Author author1 = new Author("tacgia1", "tg1@gmail.com", 'm');
        Author author2 = new Author("tacgia2", "tg2@gmail.com", 'f');
        Author author3 = new Author("tacgia3", "tg3@gmail.com", 'm');
        Author author4 = new Author("tacgia4", "tg4@gmail.com", 'f');
        Author author5 = new Author("tacgia5", "tg5@gmail.com", 'm');
        Author author6 = new Author("tacgia6", "tg6@gmail.com", 'f');

        // in thử
        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);
        System.out.println(author4);
        System.out.println(author5);
        System.out.println(author6);

        //tạo 3 list author con
        ArrayList<Author> authorlist1 = new ArrayList<>();
        authorlist1.add(author1);
        authorlist1.add(author2);

        ArrayList<Author> authorlist2 = new ArrayList<>();
        authorlist2.add(author3);
        authorlist2.add(author4);

        ArrayList<Author> authorlist3 = new ArrayList<>();
        authorlist3.add(author5);
        authorlist3.add(author6);

        Book book1 = new Book("Book 1",  authorlist1, 1000, 5);
        Book book2 = new Book("Book 2",  authorlist2, 2000, 6);
        Book book3 = new Book("Book 3",  authorlist3, 3000, 7);

        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);

        //tạo ArrayList Books
        ArrayList<Book> bookList = new ArrayList<Book>();
        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);
        
        // Return the ArrayList of books
        return bookList;

    }
}
